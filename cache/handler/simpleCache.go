package handler

import (
	"fmt"
	pb "gitlab.com/sergeysynergy-go/twinkit/cache/proto"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

// SimpleCache is a simple cache handler for dev and test cases.
type SimpleCache struct {
	jobsNumber 		int
	mu 				sync.RWMutex
}

func (sc *SimpleCache) increaseJobsNumber() int {
	sc.mu.Lock()
	sc.jobsNumber += 1
	jobNum := sc.jobsNumber
	sc.mu.Unlock()
	return jobNum
}

func (sc *SimpleCache) handleRequest(wg *sync.WaitGroup, reqNum, jobNum int, out chan<- *pb.Response)  {
	defer wg.Done()
	duration := rand.Intn(jobNum + 1)
	msg := fmt.Sprintf("REQ %04d JOB %04d : sleeping for %d seconds", reqNum, jobNum, duration)
	fmt.Println(msg)
	time.Sleep(time.Duration(duration)*time.Second)
	msg = fmt.Sprintf("REQ %04d JOB %04d : slept for %d seconds", reqNum, jobNum, duration)
	fmt.Println(msg)
	resp := &pb.Response{
		Data: 	msg,
		Len: 	int64(len(msg)),
		URL:  	"simple.cache/" + strconv.Itoa(reqNum) + "/" + strconv.Itoa(jobNum),
		Error: 	"",
	}
	out <- resp
}

func (sc *SimpleCache) HandleRequest(reqNum, nor int, out chan<- *pb.Response, quit chan<- struct{}) {
	wg := &sync.WaitGroup{}
	for i := 0; i < nor; i++ {
		jobNum := sc.increaseJobsNumber()
		wg.Add(1)
		go sc.handleRequest(wg, reqNum, jobNum, out)
	}
	wg.Wait()
	quit <- struct{}{}
}
