package main

import (
	"bytes"
	"flag"
	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/sergeysynergy-go/twinkit/cache/handler"
	pb "gitlab.com/sergeysynergy-go/twinkit/cache/proto"
	"google.golang.org/grpc"
	"math/rand"
	"net"
	"net/http"
	"path/filepath"
	"strconv"
	"time"
)

var (
	numberOfRequests, minTimeout, maxTimeout, maxLockTimeout, portNumber, maxRequestsNumber int
	configPath, redisServer string
)

func flags() {
	flag.StringVar(&configPath, "config", "", "path to configuration file")
	flag.Parse()
}

func config() {
	// Default config
	var yamlConf = []byte(`
NumberOfRequests: 	2
MinTimeout:			10
MaxTimeout: 		100
MaxLockTimeout: 	20
PortNumber: 		8082
MaxRequestsNumber:  10000
RedisServer:        "localhost:6379"
`)
	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(yamlConf))
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
	viper.SetConfigName(filepath.Base(configPath))
	viper.AddConfigPath(filepath.Dir(configPath))
	err = viper.MergeInConfig()
	if err != nil {
		log.Warn().Msg("config file not found, using default settings")
	}

	numberOfRequests = viper.GetInt("NumberOfRequests")
	minTimeout = viper.GetInt("MinTimeout")
	maxTimeout = viper.GetInt("MaxTimeout")
	maxLockTimeout = viper.GetInt("MaxLockTimeout")
	portNumber = viper.GetInt("PortNumber")
	maxRequestsNumber = viper.GetInt("MaxRequestsNumber")
	redisServer = viper.GetString("RedisServer")

	if numberOfRequests <= 0 {
		log.Warn().Msg("wrong numberOfRequests number, set to default value")
		numberOfRequests = 3
	}
	if minTimeout <= 0 {
		log.Warn().Msg("wrong minTimeout duration, set to default value")
		minTimeout = 10
	}
	if maxTimeout <= 0 {
		log.Warn().Msg("wrong maxTimeout duration, set to default value")
		maxTimeout = 100
	}
	if maxRequestsNumber <= 0 {
		log.Warn().Msg("wrong maxRequestsNumber number, set to default value")
		maxRequestsNumber = 10000
	}
	if maxLockTimeout <= 0 {
		log.Warn().Msg("wrong maxLockTimeout duration, set to default value")
		maxLockTimeout = 20
	}
}

func main()  {
	flags()
	config()
	rand.Seed(time.Now().UTC().UnixNano())
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	//zerolog.SetGlobalLevel(zerolog.InfoLevel)

	// Init remote data fetcher handler to use with cacher.
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout: 120 * time.Second,
			KeepAlive: 120 * time.Second,
		}).DialContext,
		MaxIdleConns: 100,
		IdleConnTimeout: 120 * time.Second,
		TLSHandshakeTimeout: 10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
	remote := &handler.RemoteData{
		Client: &http.Client{
			Timeout: time.Second * time.Duration(maxLockTimeout),
			Transport: transport,
		},
	}

	// Init Redis cache handler to use with cacheServer.
	redisCache := &handler.RedisCache{
		Rdb: 			redis.NewClient(&redis.Options{
			Addr:     redisServer,
			Password: "", // no password set
			DB:       0,  // use default DB
		}),
		Dtf: 			remote,  // handler to use for fetching data
		MaxLockTimeout: maxLockTimeout,
		MinTimeout: 	minTimeout,
		MaxTimeout: 	maxTimeout,
		URLs: 			[]string{  // list of URLs to use for random data downloading
			"https://golang.org",
			"https://www.google.com",
			"https://www.atlasian.com",
			"https://www.bbc.co.uk",
			"https://www.github.com",
			"https://www.gitlab.com",
			"https://www.duckduckgo.com",
			"https://www.twitter.com",
			"https://www.facebook.com",
		},
	}

	// Init and register cacheServer.
	cacheServer := &cacheServer{
		MaxRequests: 	maxRequestsNumber,
		Cache: 			redisCache,  // handler to use for caching data
	}
	server := grpc.NewServer()
	pb.RegisterCacheServer(server, cacheServer)

	port := ":" + strconv.Itoa(portNumber)
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal().Msgf("cant listen port %v", err)
	}
	log.Info().Msgf("starting server at %s", port)
	err = server.Serve(lis)
	if err != nil {
		log.Fatal().Msgf("starting server error %v", err)
	}
}
